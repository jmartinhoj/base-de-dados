CREATE TABLE users (
	username VARCHAR(50),
	password VARCHAR(50),
	editor	 BOOL,
	PRIMARY KEY(username)
);

CREATE TABLE album (
	id		 serial,
	name		 VARCHAR(100),
	launching_date DATE,
	description	 VARCHAR(512),
	PRIMARY KEY(id)
);

CREATE TABLE music (
	id	 serial,
	name	 VARCHAR(100),
	lyrics	 VARCHAR(512),
	history VARCHAR(512),
	PRIMARY KEY(id)
);

CREATE TABLE critic (
	text		 VARCHAR(512),
	pontuation	 INTEGER,
	album_id	 INTEGER,
	users_username VARCHAR(50),
	PRIMARY KEY(album_id,users_username)
);

CREATE TABLE artist (
	id	 serial,
	name VARCHAR(300),
	bio	 VARCHAR(512),
	PRIMARY KEY(id)
);

CREATE TABLE concert (
	id		 serial,
	location	 VARCHAR(512),
	concert_date DATE,
	PRIMARY KEY(id)
);

CREATE TABLE music_file (
	file		 BYTEA,
	music_id	 INTEGER,
	users_username VARCHAR(50),
	PRIMARY KEY(music_id,users_username)
);

CREATE TABLE musician (
	birthday	 DATE,
	artist_id INTEGER,
	PRIMARY KEY(artist_id)
);

CREATE TABLE band (
	creation_date DATE,
	artist_id	 INTEGER,
	PRIMARY KEY(artist_id)
);

CREATE TABLE record_company (
	id	 serial,
	name VARCHAR(512),
	PRIMARY KEY(id)
);

CREATE TABLE playlist (
	id		 serial,
	nome		 VARCHAR(512),
	public	 BOOL,
	users_username VARCHAR(50),
	PRIMARY KEY(id)
);

CREATE TABLE sequence (
	sequence_number	 INTEGER NOT NULL,
	music_id		 INTEGER NOT NULL,
	playlist_id		 INTEGER,
	PRIMARY KEY(sequence_number, music_id, playlist_id)
);

CREATE TABLE artist_concert (
	artist_id	 INTEGER,
	concert_id INTEGER,
	PRIMARY KEY(artist_id,concert_id)
);

CREATE TABLE album_artist (
	album_id	 INTEGER,
	artist_id INTEGER,
	PRIMARY KEY(album_id,artist_id)
);

CREATE TABLE users_music_file (
	users_username		 VARCHAR(50),
	music_file_music_id	 INTEGER,
	music_file_users_username VARCHAR(50),
	PRIMARY KEY(users_username,music_file_music_id,music_file_users_username)
);

CREATE TABLE music_record_company (
	music_id		 INTEGER,
	record_company_id INTEGER,
	PRIMARY KEY(music_id,record_company_id)
);

CREATE TABLE music_artist (
	music_id	 INTEGER,
	artist_id INTEGER,
	PRIMARY KEY(music_id,artist_id)
);

CREATE TABLE band_musician (
	band_artist_id	 INTEGER,
	musician_artist_id INTEGER,
	PRIMARY KEY(band_artist_id,musician_artist_id)
);

CREATE TABLE music_artist_1 (
	music_id	 INTEGER,
	artist_id INTEGER,
	PRIMARY KEY(music_id,artist_id)
);

CREATE TABLE album_music (
	album_id INTEGER NOT NULL,
	music_id INTEGER,
	PRIMARY KEY(music_id)
);

ALTER TABLE critic ADD CONSTRAINT critic_fk1 FOREIGN KEY (album_id) REFERENCES album(id);
ALTER TABLE critic ADD CONSTRAINT critic_fk2 FOREIGN KEY (users_username) REFERENCES users(username);
ALTER TABLE music_file ADD CONSTRAINT music_file_fk1 FOREIGN KEY (music_id) REFERENCES music(id);
ALTER TABLE music_file ADD CONSTRAINT music_file_fk2 FOREIGN KEY (users_username) REFERENCES users(username);
ALTER TABLE musician ADD CONSTRAINT musician_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE band ADD CONSTRAINT band_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE playlist ADD CONSTRAINT playlist_fk1 FOREIGN KEY (users_username) REFERENCES users(username);
ALTER TABLE sequence ADD CONSTRAINT sequence_fk1 FOREIGN KEY (music_id) REFERENCES music(id);
ALTER TABLE sequence ADD CONSTRAINT sequence_fk2 FOREIGN KEY (playlist_id) REFERENCES playlist(id);
ALTER TABLE artist_concert ADD CONSTRAINT artist_concert_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE artist_concert ADD CONSTRAINT artist_concert_fk2 FOREIGN KEY (concert_id) REFERENCES concert(id);
ALTER TABLE album_artist ADD CONSTRAINT album_artist_fk1 FOREIGN KEY (album_id) REFERENCES album(id);
ALTER TABLE album_artist ADD CONSTRAINT album_artist_fk2 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE users_music_file ADD CONSTRAINT users_music_file_fk1 FOREIGN KEY (users_username) REFERENCES users(username);
ALTER TABLE users_music_file ADD CONSTRAINT users_music_file_fk2 FOREIGN KEY (music_file_music_id, music_file_users_username) REFERENCES music_file(music_id, users_username);
ALTER TABLE music_record_company ADD CONSTRAINT music_record_company_fk1 FOREIGN KEY (music_id) REFERENCES music(id);
ALTER TABLE music_record_company ADD CONSTRAINT music_record_company_fk2 FOREIGN KEY (record_company_id) REFERENCES record_company(id);
ALTER TABLE music_artist ADD CONSTRAINT music_artist_fk1 FOREIGN KEY (music_id) REFERENCES music(id);
ALTER TABLE music_artist ADD CONSTRAINT music_artist_fk2 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE band_musician ADD CONSTRAINT band_musician_fk1 FOREIGN KEY (band_artist_id) REFERENCES band(artist_id);
ALTER TABLE band_musician ADD CONSTRAINT band_musician_fk2 FOREIGN KEY (musician_artist_id) REFERENCES musician(artist_id);
ALTER TABLE music_artist_1 ADD CONSTRAINT music_artist_1_fk1 FOREIGN KEY (music_id) REFERENCES music(id);
ALTER TABLE music_artist_1 ADD CONSTRAINT music_artist_1_fk2 FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE album_music ADD CONSTRAINT album_music_fk1 FOREIGN KEY (album_id) REFERENCES album(id);
ALTER TABLE album_music ADD CONSTRAINT album_music_fk2 FOREIGN KEY (music_id) REFERENCES music(id);
