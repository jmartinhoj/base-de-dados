import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.sql.Date;
import java.util.ArrayList;
import java.sql.ResultSet;

public class AddAlbum extends JFrame{
    private JPanel panel1;
    private JTextField NameField;
    private JTextField LaunchingDateField;
    private JButton OKButton;
    private JTextArea DescriptionField;
    private JButton addButton;
    private JList MusicsList;
    private JButton backButton;

    public AddAlbum(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        DefaultListModel musicsModel = new DefaultListModel();
        ResultSet rs_musics = Main.list_musics();
        MusicsList.setModel(musicsModel);
        ArrayList<Integer> ids_musics = nameToList(musicsModel, rs_musics, "name", "id");
        ArrayList<Integer> ids_choosen = new ArrayList<>();
        //Main.users_musics("ines"); FALTA VER AS MUSICAS DO USER E PRINTA LAS PARA ELE ESCOLHER QUAIS QUER POR NO ALBUM
        //list1.add()
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Date launching_date = null;
                if(!LaunchingDateField.getText().equals("")) {
                    launching_date = Main.string_to_date(LaunchingDateField.getText());
                }
                Main.add_album(NameField.getText(),  launching_date, DescriptionField.getText());
                int album_id = Main.get_album_id();
                for (int i=0; i<ids_choosen.size(); i++) {
                    Main.add_album_music(album_id, ids_choosen.get(i));
                }
                NameField.setText("");
                LaunchingDateField.setText("");
                DescriptionField.setText("");
                musicsModel.removeAllElements();
            }
        });

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ids_choosen.add(ids_musics.get(MusicsList.getSelectedIndex()));
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Insercao(username);
            }
        });
    }
    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
