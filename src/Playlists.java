import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Playlists extends JFrame{
    private JPanel panel1;
    private JList playlistsList;
    private JButton addNewPlaylistButton;
    private JButton seePlaylistButton;
    private JButton backButton;

    public Playlists(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        DefaultListModel playlistsModel = new DefaultListModel();
        ResultSet rs_playlists = Main.list_users_playlists(username);
        playlistsList.setModel(playlistsModel);
        System.out.println("oi");
        ArrayList<Integer> ids_playlists = nameToList(playlistsModel, rs_playlists, "nome", "id");

        addNewPlaylistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new AddPlaylist(username);
            }
        });

        seePlaylistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new PlaylistDetails(ids_playlists.get(playlistsList.getSelectedIndex()), username);
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Menu(username);
            }
        });
    }

    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        System.out.println("aqui dentro");
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    System.out.println("playlits: " + rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            System.out.println("null");
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
