import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.xml.transform.Result;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;



public class Menu  extends JFrame{
    private JPanel panel1;
    private JButton pesquisarButton;
    private JButton inserirButton;
    private JButton privilegesButton;
    private JLabel errorMessage;
    private JButton playlistsButton;

    public Menu(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        errorMessage.setVisible(false);
        if(Main.has_privileges(username)) {
            inserirButton.setEnabled(true);
            privilegesButton.setEnabled(true);
        }
        pesquisarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Pesquisa(username);
            }
        });
        inserirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Insercao(username);
            }
        });
        privilegesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (Main.has_privileges(username)) {
                    setVisible(false);
                    new Privileges(username);
                } else {
                    errorMessage.setVisible(true);
                }
            }
        });
        playlistsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Playlists(username);
            }
        });
    }
}
