import javax.swing.*;
import javax.xml.transform.Result;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Pesquisa extends JFrame {

    private JPanel panel1;
    private JTextField search;
    private JRadioButton artistRadioButton;
    private JRadioButton musicRadioButton;
    private JRadioButton albumRadioButton;
    private JButton searchButton;
    private JList searchResults;
    private JButton moreInfo;
    private JButton backButton;
    ArrayList<Integer> ids = new ArrayList<>();

    public Pesquisa(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DefaultListModel model = new DefaultListModel();
                ids.clear();
                if(artistRadioButton.isSelected() && !search.getText().equals("")) {
                    ResultSet rs = Main.search_artist(search.getText());
                    int i = 0;
                    try {
                        searchResults.setModel(model);
                        while (rs.next()) {
                            model.add(i, rs.getString("name"));
                            ids.add(rs.getInt("id"));
                            i++;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                else if (musicRadioButton.isSelected() && !search.getText().equals("")) {
                    ResultSet rs = Main.search_music(search.getText());
                    int i = 0;
                    try {
                        searchResults.setModel(model);
                        while (rs.next()) {
                            model.add(i, rs.getString("name"));
                            ids.add(rs.getInt("id"));
                            i++;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                else if (albumRadioButton.isSelected() && !search.getText().equals("")) {
                    ResultSet rs = Main.search_album(search.getText());
                    int i = 0;
                    try {
                        searchResults.setModel(model);
                        while (rs.next()) {
                            model.add(i, rs.getString("name"));
                            ids.add(rs.getInt("id"));
                            i++;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        moreInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(searchResults.getSelectedIndex() != -1 && artistRadioButton.isSelected()) {
                    setVisible(false);
                    new ArtistDetails(ids.get(searchResults.getSelectedIndex()), username);
                }
                else if (searchResults.getSelectedIndex() != -1 && musicRadioButton.isSelected()){
                    setVisible(false);
                    new MusicDetails(ids.get(searchResults.getSelectedIndex()), username);
                }
                else if (searchResults.getSelectedIndex() != -1 && albumRadioButton.isSelected()){
                    setVisible(false);
                    new AlbumDetails(ids.get(searchResults.getSelectedIndex()), username);
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Menu(username);
            }
        });
    }
}
