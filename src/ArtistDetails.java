import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ArtistDetails extends JFrame {
    private JPanel panel1;
    private JList bandsList;
    private JList playedMusicsList;
    private JList albumList;
    private JTextArea bio;
    private JTextField artistName;
    private JTextField birthday;
    private JLabel band;
    private JList composedMusicsList;
    private JButton editarButton;
    private JLabel errorLabel;
    private JButton backButton;
    public static final int BAND = 0;
    public static final int MUSICIAN = 1;
    public int type;
    DefaultListModel albumModel = new DefaultListModel();
    DefaultListModel playedMusicModel = new DefaultListModel();
    DefaultListModel composedMusicModel = new DefaultListModel();
    DefaultListModel bandsModel = new DefaultListModel();
    ResultSet rs;
    ResultSet rs_album;
    ResultSet rs_composed_musics;
    ResultSet rs_played_musics;
    ResultSet rs_bands;
    ArrayList<Integer> ids_album;
    ArrayList<Integer> ids_composed_music;
    ArrayList<Integer> ids_played_music;
    ArrayList<Integer> ids_bands;
    private int id;
    private String username;


    public ArtistDetails (int id, String username) {
        this.id = id;
        this.username = username;
        errorLabel.setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        rs_bands = Main.musician_bands(id);
        type = MUSICIAN;
        if(rs_bands == null) {
            type = BAND;
            band.setText("Band Elements:");
            rs_bands = Main.band_elements(id);
            birthday.setText("Creation Date:");
        }
        update();

        setVisible(true);
        playedMusicsList.addMouseListener(new CustomMouseClicker(ids_played_music, playedMusicsList, id, "artist", "played", username, this));

        composedMusicsList.addMouseListener(new CustomMouseClicker(ids_composed_music, composedMusicsList, id, "artist", "composed", username, this));

        albumList.addMouseListener(new CustomMouseClicker(ids_album, albumList, id, "artist", "album", username, this));

        bandsList.addMouseListener(new CustomMouseClicker(ids_bands, bandsList, id, "artist", "band", username, this));
        editarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("CLICOU");
                    PreparedStatement stmt = Main.c.prepareStatement("UPDATE artist SET name = ?, bio = ? where id = ?");
                    stmt.setString(1, artistName.getText());
                    stmt.setString(2, bio.getText());
                    stmt.setInt(3, id);
                    stmt.executeUpdate();
                    Main.c.commit();
                    if (type == MUSICIAN) {
                        stmt = Main.c.prepareStatement("UPDATE musician SET birthday = ? where artist_id = ?");
                    }
                    else {
                        stmt = Main.c.prepareStatement("UPDATE band SET birthday = ? where artist_id = ?");
                    }
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setLenient(false);
                    stmt.setDate(1, new java.sql.Date(df.parse(birthday.getText()).getTime()));
                    stmt.setInt(2, id);
                    stmt.executeUpdate();
                    Main.c.commit();
                    update();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    try {
                        Main.c.rollback();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Pesquisa(username);
            }
        });
    }

    public static String cleanString(String string) {
        if(string == null) {
            return "sem informação disponível";
        }
        return string;
    }

    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        model.clear();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            if (name_column.equals("music")) {
                System.out.println("no musics");
            }
            else if (name_column.equalsIgnoreCase("album")) {
                System.out.println("no albums");
            }
            else {
                ex.printStackTrace();
            }
            return null;
        }
    }

    public void update() {
        System.out.println("WILL UPDATE");
        String date;
        try {
            rs = Main.artist_details(id);
            rs_album = Main.artist_albums(id);
            rs_composed_musics = Main.artist_musics(id, "composed");
            rs_played_musics = Main.artist_musics(id, "played");
            if(type == BAND) {
                System.out.println("é banda");
                date = "creation_date";
                rs_bands = Main.band_elements(id);
            }
            else {
                System.out.println("é artista");
                date = "birthday";
                rs_bands = Main.musician_bands(id);
            }
            bio.setText(cleanString(rs.getString("bio")));
            artistName.setText(cleanString(rs.getString("name")));
            if(rs_bands.getDate(date) != null) {
                birthday.setText(cleanString(rs_bands.getDate(date).toString()));
            }
            else {
                birthday.setText("sem data disponível");
            }
            albumList.setModel(albumModel);
            bandsList.setModel(bandsModel);
            composedMusicsList.setModel(composedMusicModel);
            playedMusicsList.setModel(playedMusicModel);
            bandsList.setModel(bandsModel);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        ids_album = nameToList(albumModel, rs_album, "name", "id");
        ids_composed_music = nameToList(composedMusicModel, rs_composed_musics, "name", "id");
        ids_played_music = nameToList(playedMusicModel, rs_played_musics, "name", "id");
        ids_bands = nameToList(bandsModel, rs_bands, "name", "id");

    }
    public void error() {
        errorLabel.setVisible(true);
    }
}
