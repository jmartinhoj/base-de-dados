import javax.swing.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Date;
import java.sql.Date;
import java.sql.Date;


public class Main {
    public static Connection c;
    public static int fileName = 0;

    public static void receive_file(String username, int music_id, String path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(path));
            PreparedStatement stmt = c.prepareStatement("INSERT INTO music_file (file, music_id, users_username) VALUES (?, ?, ?)");
            stmt.setBinaryStream(1, fis);
            stmt.setInt(2, music_id);
            stmt.setString(3, username);
            stmt.executeUpdate();
            c.commit();
        } catch (Exception e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

    }

    public static void download_file(ResultSet rs) {
        try {
            byte[] fileBytes = rs.getBytes(1);
            FileOutputStream fos = new FileOutputStream(rs.getString("name") + ".mp3");
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            //No of bytes read in one read() call
            bos.write(fileBytes);
            bos.flush();
            System.out.println("Downloaded!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static ResultSet check_music_file(String username, int music_id) {
        try {
            PreparedStatement st = c.prepareStatement("SELECT file, m.name FROM music_file, music m WHERE users_username = ? and music_id = ? and m.id = music_id");
            st.setString(1, username);
            st.setInt(2, music_id);
            ResultSet rs = st.executeQuery();
            if(rs.next()) {
                return rs;
            }
            else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        }
    }

    public static void share_music(String owner_username, String username, int music_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO users_music_file (users_username, music_file_music_id, music_file_users_username) VALUES (?, ?, ?)");
            stmt.setString(1, username);
            stmt.setInt(2, music_id);
            stmt.setString(3, owner_username);
            stmt.executeUpdate();
            c.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static ResultSet check_shared_music_file(String username, int music_id) {
        try {
            PreparedStatement st = c.prepareStatement("SELECT x.file, m.name, x.users_username FROM users_music_file u, music_file x, music m WHERE u.users_username = ? and music_file_music_id = ? and x.music_id = music_file_music_id and m.id = x.music_id");
            st.setString(1, username);
            st.setInt(2, music_id);
            ResultSet rs = st.executeQuery();
            if(rs.next()) {
                return rs;
            }
            else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        }
    }

    public static int login(String user, String password) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT username, password FROM users WHERE username = ?;");
            stmt.setString(1, user);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                if (password.equals(rs.getString("password"))) {
                    return 1;
                }
            }
            return 2;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int register(String user, String password) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("INSERT INTO users (username, password, editor) VALUES (?, ?, true);");
            stmt.setString(1, user);
            stmt.setString(2, password);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return 1;
        }
        catch (SQLException e) {
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return 0;
        }
    }
    public static ResultSet search_music(String word) {
        PreparedStatement stmt;
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT name, id FROM music WHERE name like \'%" + word + "%\';");
            ResultSet rs = stmt.executeQuery();
            /*ArrayList<String[]> results = new ArrayList();
            String[] array = new String[2];
            int previous_id = -1;
            int id;
            String aux = "";
            while (rs.next()) {
                id = rs.getInt(3);
                if(id != previous_id) { //primeiro artista de uma musica
                    if (previous_id != -1) { //caso nao seja a primeira musica de todos
                        array[0] = aux;
                        array[1] = Integer.toString(rs.getInt(3));
                        results.add(array);
                    }
                    aux += rs.getString(1) + " - " + rs.getString(2);
                    previous_id = id;
                }
                else { //se aparecer mais um artista para uma musica que ja tem um artista
                    aux += ", " + rs.getString(2);
                }*/
            return rs;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet search_artist(String word) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT name, id FROM artist WHERE name like \'%"+ word +"%\' ");
            ArrayList<String[]> results = new ArrayList();
            String[] array = new String[2];
            String aux = "";
            ResultSet rs = stmt.executeQuery();
            /*while (rs.next()) {
                aux = rs.getString(1);
                array[0] = aux;
                array[1] = "" +  rs.getInt(2);
                results.add(array);
            }*/
            return rs;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet search_album(String word) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT name, id FROM album WHERE name like \'%"+ word +"%\' ");
            ArrayList<String[]> results = new ArrayList();
            String[] array = new String[2];
            String aux = "";
            ResultSet rs = stmt.executeQuery();
            /*while (rs.next()) {
                aux = rs.getString(1) + ", " + rs.getString(2);
                array[0] = aux;
                array[1] = Integer.toString(rs.getInt(3));
                results.add(array);
            }*/

            return rs;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    private ArrayList<String[]> list_concerts_by_location(String word) {
        PreparedStatement stmt;
        try { //    CONCERTO N DEVERIA TER ID?
            stmt = c.prepareStatement("SELECT artist.name, concert.location, concert.id FROM artist_concert, artist, concert WHERE concert.location like %?% and artist_concert.artist_id == artist.id and artist_concert.concert_id = concert.id;");
            stmt.setString(1, word);
            ArrayList<String[]> results = new ArrayList<>();
            String[] array = new String[2];
            String aux = "";
            int count = 0;
            ResultSet rs = stmt.executeQuery();
            while (rs.next() && (count < 5)) {
                aux = "AddConcert of " + rs.getString(1) + " in " + rs.getString(2);
                array[0] = aux;
                array[1] = rs.getString(3);
                results.add(array);
                count ++;
            }
            return results;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static Boolean has_privileges(String username) { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT editor FROM users WHERE username = ?;");
            stmt.setString(1,username);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            boolean p = rs.getBoolean("editor");
            System.out.println("dentro da funcao: privileges: " + p);
            if (p) {
                System.out.println("user tem permissao p dar direitos");
                return true;
            }
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static Boolean give_privileges(String username) { //retorna true se atribuir os privilegios sem problemas, false caso contrário
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("UPDATE users SET editor = true WHERE username = ?;");
            stmt.setString(1, username);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static ArrayList<String> list_users(String username) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT username FROM users WHERE NOT username = ?;");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            ArrayList<String> results = new ArrayList();
            while (rs.next()) {
                results.add(rs.getString(1));
            }
            return results;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet list_artists() {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, id FROM artist;");
            ResultSet rs = stmt.executeQuery();
            ArrayList<String> results = new ArrayList();
            if (rs.next()) {
                return rs;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet list_users_musics(String username) { //retorna todas as musicas de um determinado user AINDA N FUNCIONA ESPERAR Q O UPLOAD FUNCIONE PARA TESTAR
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT music.name, music.id " +
                    "FROM music_file, users_music_file, music " +
                    "WHERE users_music_file.users_username = ? and users_music_file.music_file_music_id = music_file.music_id and music_file.music_id = music.id;");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet list_users_playlists(String username) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT playlist.nome, playlist.id " +
                    "FROM playlist " +
                    "WHERE playlist.users_username = ? or playlist.public = true;");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static boolean add_playlist(String name, Boolean bool, String username) { //NAO ACABADA
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO playlist (nome, public, users_username) VALUES (?,?,?);");
            stmt.setString(1,name);
            stmt.setBoolean( 2, bool);
            stmt.setString(3, username);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_album(String name, Date launching_date, String description) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO album (name, launching_date, description) VALUES (?,?,?);");
            stmt.setString(1,name);
            stmt.setDate( 2,launching_date);
            stmt.setString(3, description);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_album_music(int album_id, int music_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO album_music (album_id, music_id) VALUES (?,?);");
            stmt.setInt(1, album_id);
            stmt.setInt( 2,music_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_artist_concert(int concert_id, int artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO artist_concert (concert_id, artist_id) VALUES (?,?);");
            stmt.setInt(1,  concert_id);
            stmt.setInt( 2, artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static Date string_to_date(String string) {
        String[] dateArray = string.split("/");
        Date date = new Date(Integer.parseInt(dateArray[0]), Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[2]));
        return date;
    }
    public static int get_playlist_last_sequence(int playlist_id) { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT max(sequence_number) FROM sequence WHERE playlist_id = ?;");
            stmt.setInt(1, playlist_id);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_playlist_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM playlist order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_concert_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM concert order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            int id = 0;
            while (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_artist_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM artist order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_music_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM music order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_musician_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM artist order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static int get_album_id() { //retorna true se tem privilegios de editor, false caso contrario
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id FROM album order by id desc limit 1;");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return 0;
        }
    }
    public static boolean add_sequence(int sequence_number, int playlist_id, int music_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO sequence (sequence_number, playlist_id, music_id) VALUES (?, ?, ?);");
            stmt.setInt(1, sequence_number);
            stmt.setInt(2, playlist_id);
            stmt.setInt(3, music_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_concert(String location, Date concert_date) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO concert (location, concert_date) VALUES (?, ?);");
            stmt.setString(1, location);
            stmt.setDate(2, concert_date);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_artist(String name, String bio) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO artist (name, bio) VALUES (?, ?);");
            stmt.setString(1,name);
            stmt.setString(2, bio);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }

    public static boolean add_band_musician(int band_artist_id, int musician_artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO band_musician (band_artist_id, musician_artist_id) VALUES (?, ?);");
            stmt.setInt(1,band_artist_id);
            stmt.setInt(2, musician_artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_music_artist(int music_id, int artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO music_artist (music_id, artist_id) VALUES (?, ?);");
            stmt.setInt(1, music_id);
            stmt.setInt(2, artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_music_artist_1(int music_id, int artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO music_artist_1 (music_id, artist_id) VALUES (?, ?);");
            stmt.setInt(1, music_id);
            stmt.setInt(2, artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_band(Date creation_date, int artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO band (creation_date, artist_id) VALUES (?, ?);");
            stmt.setDate(1, creation_date);
            stmt.setInt(2, artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_musician(Date birthday, int artist_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("INSERT INTO musician (birthday, artist_id) VALUES (?, ?);");
            stmt.setDate(1, birthday);
            stmt.setInt(2, artist_id);
            stmt.executeUpdate();
            stmt.close();
            c.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean add_music(String name, String lyrics, String history) {
        try {
            PreparedStatement ps = c.prepareStatement("INSERT INTO music (name, lyrics, history) VALUES (?, ?, ?)");
            ps.setString(1, name);
            ps.setString(2, lyrics);
            ps.setString(3, history);
            ps.executeUpdate();
            ps.close();
            c.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static  ResultSet album_musics(int album_id) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT music.name, music.id FROM album_music, music WHERE album_music.album_id = ? and album_music.music_id = music.id");
            stmt.setInt(1, album_id);
            ArrayList<String[]> results = new ArrayList<>();
            String aux = "";
            String[] array = new String[2];
            ResultSet rs = stmt.executeQuery();
            /*while (rs.next()) {
                aux = rs.getString(1);
                array[0] = aux;
                array[1] = rs.getString(2);
                results.add(array);
            }*/
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    private boolean upload_file(String username, String name, String album, String artist, Date artist_birthday, File file) { //POR FAZER
        try {
            PreparedStatement ps = c.prepareStatement("INSERT INTO music_file (username, music_name, album_name, artist_name, artist_birthday, file) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setString(1, username);
            ps.setString(2, name);
            ps.setString(3, album);
            ps.setString(4, artist);
            ps.setDate(5, artist_birthday);
            ps.setBinaryStream(6, new FileInputStream(file), (int) file.length());
            ps.execute();
            ps.close();
            c.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return false;
        }
    }

    public static ResultSet album_critics(int id) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT text, pontuation, album_id, users_username FROM critic WHERE album_id = ? order by pontuation desc;");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }

    public static ResultSet critic_average (int id) {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT avg(pontuation) \"average\" FROM critic WHERE album_id = ? group by album_id;");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }

    }

    private String edit_album_details(String detail_name, String new_info, String name, String artist_name, Date artist_birthday) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("UPDATE album SET " + detail_name + " = ? WHERE name = ? and artist_name = ? and artist_birthday = ?");
            if(detail_name.toLowerCase().equals("launching_date")) {
                stmt.setDate(1, StringtoDate(new_info));
            }
            else {
                stmt.setString(1, new_info);
            }
            stmt.setString(2, name);
            stmt.setString(3, artist_name);
            stmt.setDate(4, artist_birthday);
            stmt.executeUpdate();
            return "type/edit_album_details_status;status/success";
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return "type/edit_album_details_status;status/error";
        }
    }
    public static ResultSet music_details(int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT m.id, m.name, lyrics, history, a.name \"album\", a.id \"albumid\" FROM music m, album a, album_music WHERE m.id = ? and music_id = m.id and album_id =  a.id");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                System.out.println("XAXAXAXXAXAXAXAXAXAXAXAXAXAXAXAXA");
                return rs;
            }
            return null;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static ResultSet music_basic_details(int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id, name, lyrics, history FROM music WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                System.out.println("XAXAXAXXAXAXAXAXAXAXAXAXAXAXAXAXA");
                return rs;
            }
            return null;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }

    public static ResultSet album_details(int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT a.id, a.name, launching_date, description, m.name \"music\", m.id \"musicid\" " +
                    "FROM music m, album a, album_music WHERE a.id = ? and album_id = a.id and music_id =  m.id");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }

    public static ResultSet basic_album_details(int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT id, name, launching_date, description " +
                    "FROM album WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }

    public static ResultSet list_musics () {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT * " +
                    "FROM music ");
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }

    public static ResultSet list_bands () {
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT artist.name \"name\", artist.id \"id\" FROM band, artist WHERE band.artist_id = artist.id");
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }

    public static ResultSet list_playlist_musics(int playlist_id) { //retorna todas as musicas de um determinado user AINDA N FUNCIONA ESPERAR Q O UPLOAD FUNCIONE PARA TESTAR
        PreparedStatement stmt;
        String message2 = "";
        int count = 0;
        try {
            stmt = c.prepareStatement("SELECT music.name, music.id, sequence.sequence_number " +
                    "FROM playlist, sequence, music " +
                    "WHERE playlist.id = ? and playlist.id = sequence.playlist_id and sequence.music_id = music.id ORDER BY sequence.sequence_number;");
            stmt.setInt(1, playlist_id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
            return null;
        }
    }
    public static ResultSet playlist_details(int playlist_id, String username) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT nome, public FROM playlist WHERE playlist.id = ? and playlist.users_username = ?");
            stmt.setInt(1, playlist_id);
            stmt.setString(2, username);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                System.out.println("XAXAXAXXAXAXAXAXAXAXAXAXAXAXAXAXA");
                return rs;
            }
            return null;

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    private int edit_music_detail(String detail_name, String new_info, int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("UPDATE music SET " + detail_name + " = ? WHERE id = ?");
            stmt.setString(1, new_info);
            stmt.setInt(2, id);
            stmt.executeUpdate();
            return 1;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return 0;
        }
    }
    public static boolean write_critic(int album_id, String username, String text, int score) {
        try {
            PreparedStatement ps = c.prepareStatement("I" +
                    "");
            ps.setString(1, text);
            ps.setInt(2, score);
            ps.setInt(3, album_id);
            ps.setString(4, username);
            ps.executeUpdate();
            ps.close();
            c.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static ResultSet artist_details(int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, bio, birthday " +
                    "FROM artist " +
                    "WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static ResultSet album_artists (int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, id " +
                    "FROM album_artist, artist " +
                    "WHERE album_id = ? and artist_id = id");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static boolean removeArtistFromAlbum (int album_id, int artist_id) {
        System.out.println("-------------");
        System.out.println(album_id);
        System.out.println(artist_id);
        System.out.println("-------------");
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT COUNT(*) FROM album_artist WHERE album_id = ?");
            stmt.setInt(1, album_id);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            if(rs.next()) {
                count = rs.getInt(1);
            }
            if(count > 1) {
                stmt = c.prepareStatement("delete from album_artist where artist_id = ? and album_id = ?;");
                stmt.setInt(1, album_id);
                stmt.setInt(2, artist_id);
                stmt.executeUpdate();
                c.commit();
                return true;
            }
            return false;

        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static boolean removeMusicFromAlbum (int album_id, int music_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT COUNT(*) FROM album_music WHERE album_id = ?");
            stmt.setInt(1, album_id);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            if(rs.next()) {
                count = rs.getInt(1);
            }
            if(count > 1) {
                stmt = c.prepareStatement("DELETE FROM album_music " +
                        "WHERE album_id = ? and music_id = ?");
                stmt.setInt(1, album_id);
                stmt.setInt(2, music_id);
                stmt.executeUpdate();
                c.commit();
                return true;
            }
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }
    public static ResultSet artist_albums (int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, id " +
                    "FROM album_artist, album " +
                    "WHERE album_id = id and artist_id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static ResultSet music_artists (int id, String type) {
        String table;
        if(type.equals("composed")) {
            table = "music_artist_1";
        }
        else {
            table = "music_artist";
        }
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, id " +
                    "FROM " + table + ", artist " +
                    "WHERE artist_id = id and music_id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static void removeMusicFromArtist (int artist_id, int music_id, String type) {
        String table;
        if(type.equals("composer")) {
            table = "music_artist_1";
        }
        else {
            table = "music_artist";
        }
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("DELETE FROM " + table +
                    " WHERE artist_id = ? and music_id = ?");
            stmt.setInt(1, artist_id);
            stmt.setInt(2, music_id);
            stmt.executeUpdate();
            c.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
    public static ResultSet artist_musics (int id, String type) { //type = "composed"/"played"
        String table;
        if(type.equals("composed")) {
            table = "music_artist_1";
        }
        else {
            table = "music_artist";
        }
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT name, id " +
                    "FROM " + table + ", music " +
                    "WHERE music_id = id and artist_id = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                System.out.println("XUPAMOS");
                return rs;
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static ResultSet band_elements (int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT creation_date, name, id " +
                    "FROM  artist, band b, band_musician, musician mu " +
                    "WHERE b.artist_id = ? and band_artist_id = b.artist_id and musician_artist_id = mu.artist_id and mu.artist_id = id");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            else {
                stmt = c.prepareStatement("SELECT creation_date " +
                        "FROM  band " +
                        "WHERE b.artist_id = ?");
                stmt.setInt(1, id);
                rs = stmt.executeQuery();
                if(rs.next()) {
                    return rs;
                }
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            return null;
        }
    }
    public static void removeArtistFromBand (int musician_id, int band_id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("DELETE FROM band_musician " +
                    "WHERE band_artist_id = ? and musician_artist_id = ?");
            stmt.setInt(1, band_id);
            stmt.setInt(2, musician_id);
            stmt.executeUpdate();
            c.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
    public static ResultSet musician_bands (int id) {
        PreparedStatement stmt;
        try {
            stmt = c.prepareStatement("SELECT mu.birthday, name, id " +
                    "FROM  artist a, band b, band_musician, musician mu " +
                    "WHERE mu.artist_id = ? and musician_artist_id = mu.artist_id and band_artist_id = b.artist_id and b.artist_id = id");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs;
            }
            else {
                stmt = c.prepareStatement("SELECT mu.birthday " +
                        "FROM musician mu " +
                        "WHERE mu.artist_id = ?");
                stmt.setInt(1, id);
                rs = stmt.executeQuery();
                if(rs.next()) {
                    return rs;
                }
            }
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println( e.getClass().getName()+": "+ e.getMessage());
            return null;
        }
    }
    private Date StringtoDate(String date) {
        String[] stringDate;
        stringDate =date.split("-");
        return Date.valueOf(LocalDate.of(Integer.parseInt(stringDate[0]), Integer.parseInt(stringDate[1]), Integer.parseInt(stringDate[2])));

    }
    private void printArray(String[] s) {
        for (int i = 0; i< s.length; i++) {
            System.out.print(s[i] + "    ");
        }
    }
    public static void main(String[] args) {
        new MusicDatabase();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dropmusicbd", "postgres", "1234");
            c.setAutoCommit(false);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}