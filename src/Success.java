import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Success extends JFrame{
    private JPanel panel1;
    private JLabel MessageField;
    private JButton okButton;

    public Success(String message) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100,100,100,100);
        getContentPane().add(panel1);
        setVisible(true);
        MessageField.setText(message);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
    }
}
