import javax.swing.*;
import javax.swing.text.DateFormatter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AlbumDetails extends JFrame {
    private JPanel panel1;
    private JList musicList;
    private JList artistList;
    private JComboBox score;
    private JTextField criticText;
    private JTextField albumName;
    private JTextArea description;
    private JFormattedTextField launchingDate;
    private JList criticsList;
    private JButton editButton;
    private JLabel errorLabel;
    private JButton backButton;
    private JLabel average;
    DefaultListModel musicModel = new DefaultListModel();
    DefaultListModel artistModel = new DefaultListModel();
    DefaultListModel criticModel = new DefaultListModel();

    ResultSet rs;
    ResultSet rs_artist;
    ResultSet rs_critics;

    ArrayList<Integer> ids_artist;
    ArrayList<Integer> ids_music;

    int id;
    String username;


    public AlbumDetails(int id, String username) {
        average.setVisible(false);
        errorLabel.setVisible(false);
        this.id = id;
        this.username = username;
        if(!Main.has_privileges(username)) {
            editButton.setVisible(false);
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        musicList.setModel(musicModel);
        artistList.setModel(artistModel);
        criticsList.setModel(criticModel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100,100,500,300);
        getContentPane().add(panel1);
        update();
        setVisible(true);

        musicList.addMouseListener(new CustomMouseClicker(ids_music, musicList, id, "album", "music", username, this));

        artistList.addMouseListener(new CustomMouseClicker(ids_artist, artistList, id, "album", "artist", username, this));


        criticText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    Main.write_critic(id, username, criticText.getText(), Integer.parseInt((String)score.getSelectedItem()));
                    insertCritics(Main.album_critics(id), criticModel);
                }
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("CLICOU");
                    PreparedStatement stmt = Main.c.prepareStatement("UPDATE album SET name = ?, launching_date = ?, description = ? WHERE id = ?");
                    stmt.setString(1, albumName.getText());
                    stmt.setDate(2, new java.sql.Date(df.parse(launchingDate.getText()).getTime()));
                    stmt.setString(3, description.getText());
                    stmt.setInt(4, id);
                    stmt.executeUpdate();
                    Main.c.commit();
                    update();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    try {
                        Main.c.rollback();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Pesquisa(username);
            }
        });
    }
    private void insertCritics (ResultSet rs, DefaultListModel model) {
        model.clear();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString("pontuation") + " - " + rs.getString("text") + " by " +  rs.getString("users_username"));
                    i++;
                }
                while(rs.next());
                ResultSet rs_average = Main.critic_average(id);
                average.setVisible(true);
                average.setText("" + rs_average.getInt("average"));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void update() {
        try {
            rs = Main.album_details(id);
            if (rs == null) {
                rs = Main.basic_album_details(id);
            }
            rs_artist = Main.album_artists(id);
            rs_critics = Main.album_critics(id);
            description.setText(ArtistDetails.cleanString(rs.getString("description")));
            albumName.setText(ArtistDetails.cleanString(rs.getString("name")));
            if(rs.getDate("launching_date") != null) {
                launchingDate.setText(ArtistDetails.cleanString(rs.getDate("launching_date").toString()));
            }
            else {
                launchingDate.setText("sem data de lançamento disponível");
            }
            insertCritics(rs_critics, criticModel);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        ids_artist = ArtistDetails.nameToList(artistModel, rs_artist, "name", "id");
        ids_music = ArtistDetails.nameToList(musicModel, rs, "music", "musicid");
    }
    public void error() {
        errorLabel.setVisible(true);
    }
}
