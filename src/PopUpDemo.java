import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class PopUpDemo extends JPopupMenu {
    JMenuItem delete;
    public PopUpDemo(){
        delete = new JMenuItem("Remove");
        add(delete);
    }

    public JMenuItem getDelete() {
        return delete;
    }
}