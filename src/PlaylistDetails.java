import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;

public class PlaylistDetails extends JFrame{
    private JLabel NameField;
    private JList musicsList;
    private JPanel panel1;
    private JButton backButton;

    public PlaylistDetails(int playlist_id, String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        try {
            ResultSet playlist_details = Main.playlist_details(playlist_id, username);
            NameField.setText(playlist_details.getString(1));
            DefaultListModel musicsModel = new DefaultListModel();
            ResultSet rs_musics = Main.list_playlist_musics(playlist_id);
            musicsList.setModel(musicsModel);
            ArrayList<Integer> ids_artists = nameToList(musicsModel, rs_musics, "name", "id");
        } catch (Exception e){
            System.out.println("excepcao");
        }

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Playlists(username);
            }
        });
    }

    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString("sequence_number")+ " - " + rs.getString(name_column) );
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
