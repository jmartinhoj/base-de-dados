import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MusicDetails extends JFrame{
    private JPanel panel1;
    private JTextField musicName;
    private JList albumList;
    private JList composersList;
    private JList playersList;
    private JTextArea lyrics;
    private JTextArea history;
    private JButton editarButton;
    private JLabel errorLabel;
    private JButton downloadButton;
    private JButton uploadButton;
    private JTextField pathTextField;
    private JButton shareButton;
    private JTextField sharingUsername;
    private JButton backButton;
    private JLabel sharedBy;
    private DefaultListModel albumModel = new DefaultListModel();
    private DefaultListModel composersModel = new DefaultListModel();
    private DefaultListModel playersModel = new DefaultListModel();
    ResultSet rs;
    ResultSet rs_composed_musics;
    ResultSet rs_played_musics;
    ResultSet rs_music_file;
    ArrayList<Integer> ids_album;
    ArrayList<Integer> ids_composed_music;
    ArrayList<Integer> ids_played_music;
    int id;
    String username;

    public MusicDetails (int id, String username) {
        this.id = id;
        this.username = username;
        sharedBy.setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        errorLabel.setVisible(false);
        update();
        setVisible(true);
        albumList.addMouseListener(new CustomMouseClicker(ids_album, albumList, id, "music", "album", username, this));
        composersList.addMouseListener(new CustomMouseClicker(ids_composed_music, composersList, id, "music", "composer", username, this));
        playersList.addMouseListener(new CustomMouseClicker(ids_played_music, playersList, id, "music", "player", username, this));
        editarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("CLICOU");
                try {
                    PreparedStatement stmt = Main.c.prepareStatement("UPDATE music SET name = ?, lyrics = ?, history = ? WHERE id = ?");
                    stmt.setString(1, musicName.getText());
                    stmt.setString(2, lyrics.getText());
                    stmt.setString(3, history.getText());
                    stmt.setInt(4, id);
                    stmt.executeUpdate();
                    Main.c.commit();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                    try {
                        Main.c.rollback();
                    } catch (SQLException e2) {
                        e2.printStackTrace();
                    }
                }

            }
        });
        uploadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.receive_file(username, id, pathTextField.getText());
                update();
            }
        });
        downloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.download_file(rs_music_file);
            }
        });
        shareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.share_music(username, sharingUsername.getText(), id);
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Pesquisa(username);
            }
        });
    }
    public void update() {
        rs = Main.music_details(id);
        rs_composed_musics = Main.music_artists(id, "composed");
        rs_played_musics = Main.music_artists(id, "played");
        rs_music_file = Main.check_music_file(username, id);
        if(rs_music_file != null) {
            downloadButton.setEnabled(true);
            shareButton.setEnabled(true);
            uploadButton.setEnabled(false);
            sharedBy.setVisible(false);
        }
        else {
            rs_music_file = Main.check_shared_music_file(username, id);
            if(rs_music_file != null) {
                downloadButton.setEnabled(true);
                try {
                    sharedBy.setText("shared by " + rs_music_file.getString("users_username"));
                    sharedBy.setVisible(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            if(rs == null) {
                rs = Main.music_basic_details(id);
            }
            lyrics.setText(ArtistDetails.cleanString(rs.getString("lyrics")));
            musicName.setText(ArtistDetails.cleanString(rs.getString("name")));
            history.setText(ArtistDetails.cleanString(rs.getString("history")));
            albumList.setModel(albumModel);
            composersList.setModel(composersModel);
            playersList.setModel(playersModel);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        ids_album = ArtistDetails.nameToList(albumModel, rs, "album", "albumid");
        ids_composed_music = ArtistDetails.nameToList(composersModel, rs_composed_musics, "name", "id");
        ids_played_music = ArtistDetails.nameToList(playersModel, rs_played_musics, "name", "id");
    }
    public void error() {
        errorLabel.setVisible(true);
    }

}
