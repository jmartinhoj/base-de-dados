import javafx.beans.binding.IntegerBinding;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;

public class AddConcert extends JFrame {
    private JPanel panel1;
    private JTextField DateField;
    private JTextField LocationField;
    private JList ArtistsList;
    private JButton addButton;
    private JButton OKButton;
    private JButton backButton;
    ArrayList<Integer> ids_artists;

    public AddConcert(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        ArrayList<Integer> ids_choosen = new ArrayList<>();
        DefaultListModel artistsModel = new DefaultListModel();
        ResultSet rs_artists = Main.list_artists();
        ArtistsList.setModel(artistsModel);
        if (rs_artists != null) {
            ids_artists = nameToList(artistsModel, rs_artists, "name", "id");
        }

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ids_choosen.add(ids_artists.get(ArtistsList.getSelectedIndex()));
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Date date = Main.string_to_date(DateField.getText());
                Boolean result = Main.add_concert(LocationField.getText(), date);
                int concert_id = Main.get_concert_id();
                for (int i=0; i<ids_choosen.size(); i++) {
                    Main.add_artist_concert(concert_id, ids_choosen.get(i));
                }
                if (result) {
                    new Success("AddConcert created successfully!");
                }

            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Insercao(username);
            }
        });
    }

    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
