import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MusicDatabase extends JFrame{
    private JButton loginButton;
    private JPanel panel1;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JLabel tryAgain;
    private JButton registarButton;
    public static String username = "";

    public MusicDatabase() {
        tryAgain.setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username = usernameField.getText();
                switch(Main.login(usernameField.getText(), passwordField.getText())) {
                    case(1):
                        System.out.println("bananas");
                        setVisible(false);
                        new Menu(usernameField.getText());
                    case(2):
                        tryAgain.setVisible(true);
                }
            }
        });
        registarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username = usernameField.getText();
                switch(Main.register(usernameField.getText(), passwordField.getText())) {
                    case(1):
                        setVisible(false);
                        new Menu(usernameField.getText());
                    case(0):
                        tryAgain.setVisible(true);
                }
            }
        });
    }
}
