import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Insercao  extends JFrame{
    private JTextField TextField;
    private JRadioButton artistaRadioButton;
    private JRadioButton álbumRadioButton;
    private JRadioButton musicaRadioButton;
    private JPanel panel1;
    private JRadioButton concertRadioButton;
    private JButton backButton;

    public Insercao(String username){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        musicaRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new AddMusic(username);
            }
        });
        artistaRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new AddArtist(username);
            }
        });
        álbumRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new AddAlbum(username);
            }
        });
        concertRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new AddConcert(username);
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Menu(username);
            }
        });
    }
}
