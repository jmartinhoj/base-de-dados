import javax.swing.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Set;

public class AddMusic extends JFrame{
    private JPanel panel1;
    private JTextField NameField;
    private JLabel Lyrics;
    private JLabel Name;
    private JLabel History;
    private JButton OKButton;
    private JTextArea HistoryField;
    private JTextArea LyricsField;
    private JCheckBox yesCheckBox;
    private JCheckBox noCheckBox;
    private JLabel AddArtistLabel;
    private JList ArtistsList;
    private JButton playedButton;
    private JButton composedButton;
    private JButton backButton;
    public ArrayList<Integer> ids_artists;

    public AddMusic(String username){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        AddArtistLabel.setVisible(false);
        DefaultListModel artistsModel = new DefaultListModel();
        ResultSet rs_artists = Main.list_artists();
        ArtistsList.setModel(artistsModel);
        if (rs_artists != null) {
            ids_artists = nameToList(artistsModel, rs_artists, "name", "id");
        }
        ArrayList<Integer> id_played_by = new ArrayList<>();
        ArrayList<Integer> id_composed_by = new ArrayList<>();
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                setBounds(100,100,500,300);
                getContentPane().add(panel1);
                Boolean result = Main.add_music(NameField.getText(), LyricsField.getText(), HistoryField.getText());
                int music_id = Main.get_music_id();
                for (int i=0; i<id_played_by.size(); i++) {
                    Main.add_music_artist(music_id, id_played_by.get(i));
                }
                for (int i=0; i<id_composed_by.size(); i++) {
                    Main.add_music_artist_1(music_id, id_composed_by.get(i));
                }
                new Success("Music created successfully!");
                NameField.setText("");
                LyricsField.setText("");
                HistoryField.setText("");
                artistsModel.removeAllElements();

            }
        });


        playedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                id_played_by.add(ids_artists.get(ArtistsList.getSelectedIndex()));
            }
        });

        composedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                id_composed_by.add(ids_artists.get(ArtistsList.getSelectedIndex()));
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Insercao(username);
            }
        });
    }
    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
