import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;

public class AddArtist extends JFrame{
    private JRadioButton bandRadioButton;
    private JRadioButton personRadioButton;
    private JTextField NameField;
    private JTextArea BioField;
    private JTextField CreationDateField;
    private JTextField BirthdayField;
    private JPanel panel1;
    private JLabel CreationDateText;
    private JLabel BirthdayText;
    private JButton OKButton;
    private JButton backButton;
    private JList bandsList;
    private JButton addButton;
    private JLabel AssociateLabel;
    Boolean band = false;
    ArrayList<Integer> ids_bands = new ArrayList<>();
    ArrayList<Integer> ids_choosen = new ArrayList<>();

    public AddArtist(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        addButton.setVisible(false);
        AssociateLabel.setVisible(false);
        bandsList.setVisible(false);
        BirthdayField.setVisible(false);
        CreationDateField.setVisible(false);
        CreationDateText.setVisible(false);
        BirthdayText.setVisible(false);
        DefaultListModel bandsModel = new DefaultListModel();
        ResultSet rs_bands = Main.list_bands();
        bandsList.setModel(bandsModel);
        if (rs_bands != null) {
            ids_bands = nameToList(bandsModel, rs_bands, "name", "id");
        }

        bandRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BirthdayField.setVisible(false);
                BirthdayText.setVisible(false);
                CreationDateField.setVisible(true);
                CreationDateText.setVisible(true);
                band = true;
            }
        });
        personRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreationDateField.setVisible(false);
                CreationDateText.setVisible(false);
                BirthdayField.setVisible(true);
                BirthdayText.setVisible(true);
                AssociateLabel.setVisible(true);
                bandsList.setVisible(true);
                addButton.setVisible(true);
                band = false;
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Boolean result = Main.add_artist(NameField.getText(), BioField.getText());
                int artist_id = Main.get_artist_id();
                if (band) { //COMO E QUE SEI O ID DO ARTISTA
                    Date creation_date = null;
                    if(!CreationDateField.getText().equals("")) {
                        creation_date = Main.string_to_date(CreationDateField.getText());
                    }
                    result = Main.add_band(creation_date, artist_id);
                } else {

                    Date birthday = null;
                    if (!BirthdayField.getText().equals("")) {
                        birthday = Main.string_to_date(BirthdayField.getText());
                    }
                    result = Main.add_musician(birthday, artist_id);
                    int band_id = Main.get_band_id(); //AQUIIIIII
                    for (int i=0; i<ids_choosen.size(); i++) {
                        Main.add_band_musician(band_id, artist_id);
                    }
                }
                new Success("Album created successfully!");
                NameField.setText("");
                BioField.setText("");
                CreationDateField.setText("");
                BirthdayField.setText("");
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Insercao(username);
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ids_choosen.add(ids_bands.get(bandsList.getSelectedIndex()));
            }
        });
    }
    public static ArrayList<Integer> nameToList(DefaultListModel model, ResultSet rs, String name_column, String id_column) {
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            int i = 0;
            if(rs != null) {
                do {
                    model.add(i, rs.getString(name_column));
                    ids.add(rs.getInt(id_column));
                    i++;
                }
                while(rs.next());
            }
            return ids;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    }
