import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Privileges extends JFrame{
    private JPanel panel1;
    private JList usersList;
    private JButton OKButton;
    private JButton backButton;

    public Privileges(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        DefaultListModel usersModel = new DefaultListModel();
        ArrayList<String> existing_users = Main.list_users(username);
        usersList.setModel(usersModel);
        for (int i=0; i<existing_users.size(); i++) {
            usersModel.add(i, existing_users.get(i));
        }

        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Boolean result = Main.give_privileges(existing_users.get(usersList.getSelectedIndex()));
                if (result) {
                    new Success("Privileges given!");
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Menu(username);
            }
        });
    }
}
