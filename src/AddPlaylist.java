import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;

public class AddPlaylist extends JFrame{
    private JTextField NameField;
    private JTextField searchField;
    private JRadioButton publicRadioButton;
    private JRadioButton privateRadioButton;
    private JList MusicsList;
    private JButton ADDButton;
    private JButton OKButton;
    private JPanel panel1;
    private JButton searchButton;
    private JButton backButton;
    private JButton FinalOKButton;
    Boolean bool;

    public AddPlaylist(String username) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200,100,1000,400);
        getContentPane().add(panel1);
        setVisible(true);
        DefaultListModel musicsModel = new DefaultListModel();
        MusicsList.setModel(musicsModel);
        ArrayList<Integer> music_ids = new ArrayList<>();
        ArrayList<Integer> ids_choosen = new ArrayList<>();
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.add_playlist(NameField.getText(), bool, username);
                int playlist_id = Main.get_playlist_id();
                for (int i=0; i<ids_choosen.size(); i++) {
                    int last_seq = Main.get_playlist_last_sequence(playlist_id);
                    Main.add_sequence(last_seq + 1, playlist_id, music_ids.get(MusicsList.getSelectedIndex()));
                }
            }
        });
        publicRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                bool = true;
            }
        });
        privateRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                bool = false;
            }
        });
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                music_ids.clear();
                ResultSet rs = Main.search_music(searchField.getText());
                int i = 0;
                try {
                    while (rs.next()) {
                        musicsModel.add(i, rs.getString("name"));
                        music_ids.add(rs.getInt("id"));
                        i++;
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        ADDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MusicsList.getSelectedIndex();
                ids_choosen.add(music_ids.get(MusicsList.getSelectedIndex()));
                //System.out.println("last sequence: " + last_seq);
                //System.out.println("OLA " + playlist_id + " - " + music_ids.get(MusicsList.getSelectedIndex()) + " - " + MusicsList.getSelectedIndex());


            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                setVisible(false);
                new Playlists(username);
            }
        });
    }
}
