import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class CustomMouseClicker extends MouseAdapter {
    ArrayList ids;
    JList list;
    int id;
    String type1;
    String type2;
    String username;
    Object o;
    public CustomMouseClicker(ArrayList ids, JList list, int id, String type1, String type2, String username, Object o) {
        this.ids = ids;
        this.list = list;
        this.id = id;
        this.type1 = type1;
        this.type2 = type2;
        this.username = username;
        this.o = o;
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        JList list = (JList)e.getSource();
        if (e.getClickCount() == 2) {
            if(list.equals("artist") || type2.equals("composer") || type2.equals("player") || type2.equals("band") || type2.equals("musician")) {
                ((JFrame)o).setVisible(false);
                new ArtistDetails((int)ids.get(list.getSelectedIndex()), username);
            }
            else if (type2.equals("album")) {
                ((JFrame)o).setVisible(false);
                new AlbumDetails((int)ids.get(list.getSelectedIndex()), username);
            }
            else if (type2.equals("composed") || type2.equals("played") || type2.equals("music")) {
                ((JFrame)o).setVisible(false);
                new MusicDetails((int)ids.get(list.getSelectedIndex()), username);
            }
        }
    }
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }
    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }
    private void doPop(MouseEvent e){
        if(Main.has_privileges(username)) {
            PopUpDemo menu = new PopUpDemo();
            menu.show(e.getComponent(), e.getX(), e.getY());
            JMenuItem delete = menu.getDelete();
            delete.addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    if(type1.equals("album")) {
                        if(type2.equals("artist")) {
                            if(!Main.removeArtistFromAlbum(id, (int)ids.get(list.getSelectedIndex()))) {
                                ((AlbumDetails)o).error();
                            }
                        }
                        else if (type2.equals("music")) {
                            if(!Main.removeMusicFromAlbum(id, (int)ids.get(list.getSelectedIndex()))) {
                                ((AlbumDetails)o).error();
                            }
                        }
                        ((AlbumDetails)o).update();
                    }
                    else if (type1.equals("artist")) {
                        switch(type2) {
                            case("played"):
                                Main.removeMusicFromArtist(id, (int)ids.get(list.getSelectedIndex()), "player");
                                break;
                            case("composed"):
                                Main.removeMusicFromArtist(id, (int)ids.get(list.getSelectedIndex()), "composer");
                                break;
                            case ("band"):
                                if(((ArtistDetails)o).type == ArtistDetails.BAND) {
                                    Main.removeArtistFromBand((int)ids.get(list.getSelectedIndex()), id);
                                }
                                else {
                                    Main.removeArtistFromBand(id, (int)ids.get(list.getSelectedIndex()));
                                }
                                break;
                            case ("album"):
                                if(!Main.removeArtistFromAlbum((int)ids.get(list.getSelectedIndex()), id)) {
                                    ((ArtistDetails)o).error();
                                }
                                break;
                        }
                        ((ArtistDetails)o).update();
                    }
                    else if (type1.equals("music")) {
                        switch(type2) {
                            case("album"):
                                if(!Main.removeMusicFromAlbum((int)ids.get(list.getSelectedIndex()), id)) {
                                    ((MusicDetails)o).error();
                                }
                                break;
                            case("composer"):
                                Main.removeMusicFromArtist((int)ids.get(list.getSelectedIndex()), id, "composer");
                                break;
                            case("player"):
                                Main.removeMusicFromArtist((int)ids.get(list.getSelectedIndex()), id, "player");
                                break;
                        }
                    }
                    ((MusicDetails)o).update();
                }
            });
        }
    }
}
